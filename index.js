const arr1 = [1, 2, 7, 4];
const arr2 = [2, 3, 9, 4, 5];


const Solution = (arr1, arr2) => {
    let duplicated = [];
    let noDuplicatedIndexFounded = [];
    for (let i = 0; i < arr1.length; i++) {
        for (let q = 0; q < arr2.length; q++) {
            if (arr1[i] === arr2[q]) {
                duplicated.push(arr1[i]);
            } else {
                noDuplicatedIndexFounded.push(arr1[i]);
            }
        }
    }
    if (duplicated.length) {
        console.log(`there are duplicated number found`);
        return duplicated;
    } else {
        console.log(`there are No duplicated number found`);
        let twoArr = arr1.concat(arr2);
        let result = twoArr.sort();
        return result;
    }
}


const SecondSolution = (arr1, arr2) => {
    let dups = [];
    for (let i = 0; i < arr1.length; i++) {
        if (arr2.indexOf(arr1[i]) > -1) {
            dups.push(arr1[i])
        } else {
            console.log(`there are No duplicated number found`);
        }
    }
    return dups;
}
console.log(Solution(arr1, arr2), `First Solution`);
console.log(SecondSolution(arr1, arr2), `Second Solution`);

